---
layout: post
category: post
title: My Vipassana Experience
date: 2019-01-28 14:25:38 -0500
tags:
- meditation
published: false

---
After completing my masters and an unsuccessful effort to find a job with all the visa restrictions in the US, I decided to come back to India. It was stressful, but that wasn't the reason for me to go for a Vipassana meditation retreat. During my bachelor's degree, I meditated almost everyday for about 2 years. It helped to restraint my anger. Trust me, it's easier said than done. After graduating, I started to move to different cities quite frequently for work and life quickly got out of hand. Along with it, my almost-daily routine of meditating went away. The main reason for the retreat was my attraction towards daily meditation and making it a habit.

  
\## Easier said than done

  
I was a very angry kid. I would lose my temper on minuscule unexpected events in my life. For instance, why are we eating dinner now? Why not a little later? I can go on with a humungous list of such examples, but I hope, you get it. Suddenly, when I was 14 my mother became sick to an extent that even walking to the restroom from her bed was a huge effort for her. She was eating minute quantity of food as even eating involved effort, and she started breathing heavily. That year changed me a lot.

  
I started doing house chores more and more, I started taking care of my brother more and more, I started being in the kitchen more and more. Never thinking, even once, that my mother may not survive this. Until, one day I lost my temper again and my dad said: "You know, son, she might not live a year from today". That was devastating. I felt a pressing need to control my temper as I didn't want to be angry at random times. It got better; I was able to help it to some extent. But I just couldn't control it. Every time after being angry, whenever I would come back to my senses, I would apologise.

  
Soon after, mom got better, I moved out of my parent's place for my studies. After a year went to college, where I did a preliminary meditation course for a week during the first semester. It helped me to observe myself more whenever I was about to lose my temper. Not only anger, but other emotions also became more conscious. Since then, I have been attracted to meditation as a daily practice.

  
\**It is very easy to say (or tell) that we ourselves are responsible for our own emotional turmoils. But it is very hard to understand how this impacts our life and how we can influence it. Someone who understands can try to explain these things to someone, however one is limited by language and that's how words like mindfulness come into picture. Once understood mindfulness seems the closest word to the experience but not the complete.** However, without getting too much into philosophy, this attraction is the reason I ended up in Vipassana retreat.

  
Another reason for this particular kind of meditation was because many people I admire like Sam Harris and Yuval Noah Harari recommend Vipassana. I went with the organisation \[Dhamma\]([http://dhamma.org](http://dhamma.org "http://dhamma.org")) because one of my close friends went through their 10-day retreat and I had the pleasure to talk to them and find that they don't try to convert people into a religion.