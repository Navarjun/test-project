---
layout: post
title:  "The way we are (or become)"
date:   2013-05-03 23:15:38 +0530
category: post
---

In today’s world can we imagine ourselves living without phones ??? Of course not!!! We setup a meeting with a friend at 5, and at 5 call him/her and ask about the same. Isn’t it ?

So is it just convenience or are we seriously missing something. I feel its the latter one. I lived without a phone for the last week. It was so amazing. No alerts, no pokes, no notifications, no messages, no phones, basically no disturbances. I checked my mail whenever I wanted to, I rarely used Facebook, I talked to people with full involvement.

I was talking to someone very close to my heart today. I talked to her over the phone.(I am writing this blog too on a phone) After a long time we shared things, events, plans, ourselves.(although we talked for an hour or so, she said that I don’t have enough to talk about)

Now, think of this for a moment, you actually find time for people. You go to a fine place a restraunt, may be a beach, may be mountains, whatever place you think is the best for you. Sit there and talk. Will the person still say that you don’t have much to talk about ??? I guess not.

“Life is short, for following your heart. But too long if you don’t. The same time can be priceless being short and worthless being long. Live the life you always dreamt of, not what others dream of you.”