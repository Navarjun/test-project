---
layout: post
title:  "When I see people dreaming"
date:   2013-08-13 08:35:38 +0530
category: post
---

I see people dreaming,
Dreaming from start to the end.
Dreaming of changing the world,
Dreaming of Flying high.
But then they wake up,
to see the door slid open,
To let some people come in, and others go out.
The door closes again and take them high.
Higher than their dreams were.
Yes, they WERE, now they have vanished seeing the door open again,
and watching their seat that earns them their Ferrari,
and makes them live in a million dollar mansion,
and buys them the suits,
and gives Christmas gifts to their kids,
and get loved by spouse,
and also, get the pills to sleep.
But I also see people, fighting for their existence,
who also dream,
but see no goal,
and just know why they do what they do,
the people who are happy being someone,
and not becoming,
who go to bed every night with pleasure,
knowing what they did today made this world a better place.

And then I wonder, what if one of them never existed,
Could other exist? My conscience says no.
One of them makes other one special,
Everyone has their own way of finding the special ones.