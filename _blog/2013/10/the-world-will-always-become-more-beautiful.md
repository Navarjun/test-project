---
layout: post
title:  "The world will always become more beautiful"
date:   2013-10-05 01:12:38 +0530
category: post
---

Augmented Reality, is what we talk/think of these days. People all around the world are building technologies to make machines really come into human environment. The day is not far when the world would be a Matrix of interfaces all around you, responding and helping you all the time. Hang on! Don’t worry, the great designers won’t make them irritating.

Technology has been changing lives since long ago. There was a time when even wheel was seen as a technology. To generation of today, a touch screen is an obvious thing (yeah! it’s true, children these days go to the TV screen and swipe for the next channel, they seriously are disappointed when it doesn’t). For us, touchscreen is something that changed the whole world.

But just imagine to be able to work on something like that. That would be obvious in near future. Something that would become transparent, that would go unnoticed in near future(in our lifetimes). Then we would say that Technology is at its best.

That’s what we do at PsiPhi Labs. We take up challenges, and try to make them things possible not only by doing them better than others, but by doing them the way they should be. That’s why we are gonna change the world. And that’s why it would be a better place for you, me and everyone else. 

~ Feelings @ Google Hackathon