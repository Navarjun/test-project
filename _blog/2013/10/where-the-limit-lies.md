---
layout: post
title:  "Where the limit lies ?"
date:   2013-10-19 01:41:38 +0530
category: post
---

Shifting headquarters of Psi Phi Labs in Dehradun was a very instantanous decision for me. The idea of working the cool breeze of Himalayas has always been one of my college day fantasies. Working from Bangalore was undoubtedly great. But here it’s incredible.

I never ever thought that there are any limits to what I can achieve, and here working with an awesome team proves that again and again for me. 90+ hours of changing the world per week, are awesome. The freedom of startups is impossible to match. We don’t do things because we got to do, but because we want to.

Enjoying the peace of this place, these beautiful moonlit nights and cool breeze blowing on my face and through my hair at nights is something impossible for employees of big companies.

Freedom is living free of all the limitations that world imposes on you. Because there are none. And we really are free.