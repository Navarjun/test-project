---
layout: post
title:  "When death looked into my eyes"
date:   2013-11-19 10:10:38 +0530
category: post
tags: story
---

Climbing a mountain we thought was easy. Yes, it was indeed. But later that night, the moon shone upon the white snow. We stood there waiting for a cellphone, to call our homes and tell our parents that all was well. It was cold, very cold. Almost everything not in direct radiation of the fire, seemed to shiver. All our cellphone signals were out. But like a blessing from the God, BSNL was still transmitting. Hell, none of us had a BSNL connection.

We go into our rooms to sleep well. Well, it wasn’t late, it was just 8 in the night, still we were sleepy, climbing a mountain is tiresome. Soon, I was shivering, beyond imagination. And sooner I was out of my senses. I thought I was sleeping well. But I was feeling cold. I tried to say it, I don’t remember saying it though. When I opened my eyes, I saw long necked people, their arms extending beyond my sight like street-lamp poles on a foggy day. I don’t remember their faces, I guess I didn’t see them. Soon I realized I was dreaming. It was like the movie inception, I could feel my dream. but really this was the first time I was dreaming of such things and in this particular way. I knew there was no electricity where I was sleeping, but I saw a bright light, it was beautiful. I heard someone say something in an unrecognizable tongue.

But then those sounds were merged with some known voice. I remember someone commanding others to put on the candle. But there was no need of one where I was. Then a strong jolt went through my body, I don’t know whether in this world or beyond, but it was like a moment that taught me everything. It was amazing. I realized that I am about to die. I thought this can’t be, this can’t be so beautiful. But then, it was too soon too.

With all my force I could gather, I started dictating my last messages. Hoping that probably someone in this world would listen to them. I said them for a long time, 10-12 hours I guess. But then there was a big smash on my head, I woke up. It was difficult to breathe. I was under a ton of weight, like my ribs just holding it enough and a feather more would cause them to break. With all my strength that I could gather, I shouted to remove the load from me. And soon after that I was well, I could stand. I asked one of my friends about the time, it was merely 10:30 of the same night.

I wasn’t surprised for I spent almost a day struggling and here it was just 2 and a half hours gone. I knew I was dying. This experience is something I would never forget. Probably, this is the first time I am telling it the way I wanted to. But, it taught me one thing,
Life is a journey, enjoy it, don’t waste it doing shit. Be kind to people, as they are also going to die someday, give them a good experience to remember in their journey. But remember there is no destination for this journey. Maybe you can put a destination for a day, maybe for a week. but what after that and what if you didn’t live a week ?

I know most of you won’t believe me on this story, but trust me.
And if you can, ask those 5 of my friends, you will know.