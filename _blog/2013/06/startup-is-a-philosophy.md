---
layout: post
title:  "Start-up is a philosophy"
date:   2013-06-07 13:41:38 +0530
category: post
---

It’s not been long since I came to Bangalore and started working on Dronna . I am having great experiences here. As usual, a start-up offers more than just polish your skills and salary. In a start-up you can build yourself, not just professionally/academically but personally and spiritually. Starting to work on a start-up and burning midnight oil to see the seeds grow into a tree is more of a spiritual journey than a materialistic one. Of course, every start-up doesn’t aim to create a change, some of them are just pursuits of Money. But start-ups like Dronna, which have the dream of creating an impact so to change our lives for good, don’t ever think of just making money(we see it as the need, not a want).

When you make things you never thought you could; making things easier for thousands of lives; you realize, you realize that You are working ON a start-up not FOR it, unlike any other job you take up. Here you will experience what people are, what is their power and where they use it. Probably, I would never be as popular as other entrepreneurs out there, but the question is ‘Do I really need to be ?’ and more than that 'Do I want to be ?’. For me, the answer is no. In a start-up you know, you are not doing things for your fame or riches. You are doing it because that’s what you want to do, that’s what satisfies you. Isn’t that where a spiritual journey takes you to, Ultimate Satisfaction.

You may have great skills, good at marketing, amazing at managing your subordinates. But still, you cannot start up, if you miss a very important ingredient - Philosophy. You need to be a great philosopher in order to do it. Not everyone can believe in “Never Give Up”, “Think positive” and other phrases and great quotes. A philosopher possesses an unmatched urge to explore and learn everything.

People don’t lack strength, They lack will.