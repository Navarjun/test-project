---
layout: post
title: "The Namesake (by Jhumpa Lahiri)"
date: 2018-07-08 14:25:38 -0700
category: post
tags: ["book review"]
---

Being an Indian who moved to Boston for my post graduation, it was a lot to learn. Even after 3 years of living here, I am not done learning about lifestyle in America. I understand and relate to so many of the things in this book. The book follows a person named Gogol whose parents move from _West Bengal_ (a state in eastern India) to Boston (in Massachusetts, north-east USA) from before his birth to his middle-age.

_SPOILERS AHEAD_

My mom passed away after 2.5 years of my move, although I was in India at that time… but still I relate to Ashima and Ashoke leaving their families behind… losing all members one by one while trying to fit into American culture.

I personally feel it is very courageous of people who leave their own home to pursue “something” even when they are not specifically running away from home (like a war, in some parts of the world). It makes me wonder when people, back in Punjab, think of moving to the parts of the world, where they don't even understand the language, or understand very little of it.

Courage is probably not even the right word. It takes more than courage… it takes determination, sacrifice, curiosity and incredible endurance against loneliness. And then when all is done, you find a person to live with and make a family. You see your kids growing up and being part of the culture you can never completely be part of, trying to keep them as close to your own culture as you were. You aren’t lonely anymore, and yet you aren’t home.

Later like Gogol's parents accept that their kids will be Americans no matter what, they celebrate American festivals for them, they accept their American-ness e.g. by cooking those foods for them etc. And yet, when twice or thrice a year those ‘American’ kids are asked to be part of a Bengali party, they despise it. The loneliness comes back, but in the end you deal with it… it's too late now to change your own kids, and too soon for them to understand you. And in this way your whole life is gone.

It's an incredible book for someone who wants to empathise with immigrants or is curious about their experience. The way America looks from India, ‘the land of dreams’ idea, is nothing like it actually is. It is a trap for one generation so that their next generation can live in ‘the land of dreams’.