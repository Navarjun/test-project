---
layout: post
title:  "Visual parameter mapping in data visualisation"
date:   2018-01-10 14:25:38 -0500
category: post
---

Since the first ever data visualisation, we as humans have been trying to map data to visual features of different kinds of shapes. The use of shapes have evolved over the centuries, and so have the methods to collect data. The data is/are becoming more complex with every passing hour. The raw data from innumerable sources — satellites, telescopes, marine sensors, GPS in our phones, security systems, economics, crime, refugee crises, trade, agriculture, energy, wearables, online advertisement, photos we click, blogs we write, texts we sent — is becoming more and more immense. And so are our visualisations. In fact, human visual perception has reached it’s limits to interpret all the data we collect at once; so have our brains gave up trying to understand the numbers.

> What does it mean when Syrian Observatory of Human Rights say that about 475,000 people have been killed?
>
> For comparison, the population of Boston, Massachusetts, USA is estimated to be 617,594 in 2016; and it’s the 10th most populated city in USA. That number is also the approximate population Costa Rica and Ireland individually.
>
> Here is a visual explanation of that 475,000 people.

## Power of parameter mapping

Of course, encoding data into visual parameters has the power to explain those numbers to us humans. That power has allowed not only experts to understand data in a better way, but also the people who cannot grasp the comparisons done with numbers. The famous Florence Nightingale’s chart helped the people at that time to understand the cause of mortality with much more clearity, and as we know today that it wasn’t the first report created by her and her team.

![Rose diagram image](rose-diagram.jpeg)

There were other ways that these numbers were communicated with the administration, but it was hard for anyone to pin point the cause. So visual parameter mapping made a huge difference to the soldiers in the field in those days.

It can be argued that the field of data visualisation exists just so we can find ways to map data values to the visual parameters in an effective way. It doesn’t always work the way we would like it to work, that’s why we need to design the visualisations.

## Non-visual parameter mapping

Visual parameter mapping is not always the best way to represent the data. It’s one of the ways that visualisation creators represent data. Although most data visualisations are done by visual parameter mapping one way or the other, still there can be cases where parameter mapping is not present in the visual representation at all. To start the discussion, I would like to mention the sound of wikipedia, which uses visual parameter mapping but not for every part of the visualisation. In fact, the project can be experienced even without a visual interface (without a screen).

> **Sidenote**
> Link visual creation from data is called data visualisation, the audio creation from data is called data sonification?

Let’s consider [IBM News Explorer][1], the complex network visualisation in the middle of the page is completely non-deterministic—every time we load the page the same data would look different. The parameter mapping is done behind the scenes that further calculates the indeterministic layout that we finally see on the screen. It is not obvious how to understand data from the visual encoding being used. The closer nodes are more closely related and the farther ones are less. But how closely related are they, how do we even know the measure of closeness? Nevertheless, it helps us navigate through the innumerable articles that can be there on the internet about any given topic.

[1]: http://news-explorer.mybluemix.net/?query=Science&type=unconstrained