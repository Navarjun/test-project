---
layout: post
title:  "In the afterlife"
date:   2018-04-20 14:25:38 -0700
category: post
---

In the afterlife, you don’t really exist. Your soul, so to say, exists. But it is still looking for the purpose. The life-long question of ‘Who am I?’ is still not answered. However, a different question ‘Does God exist?’ or ‘What is God like?’ is answered. Surprisingly for the soul it is not answered by God.

When you die, the soul goes into a different world where it meets up with other souls. It doesn’t really ‘go’ to a different world, it just realises it’s existence in the other world. During the transition the soul feels like it’s falling on to a heap of other souls. But as the ground gets closer, it feels just like falling on a planet like Earth which is over-populated with souls. Getting even closer reveals it’s not really over-populated, just that souls are connected somehow.

Finally, your soul falls onto the planet. Your soul looks at the people who pick you up. Your soul tries to ask them usual questions that any surprised person would ask: ‘Who are you?’, ‘Where am I?’. But they don’t seem to understand your language. Eventually, the soul decides to learn their language to survive. As your soul progresses, one day you ask: ‘how did I come here?’ Their response: ‘You mean, how were you born?’ They answer by saying: ‘God gave you to us’.

This makes you wonder as by that time, you don’t really remember anything about how you ended up there. The cognitive effort of learning a completely new language of another world, all by yourself, was so much that the brain found it more efficient to just forget other things. You aren’t you anymore, your soul is you. The question is answered but you don’t understand the answer anymore. The belief that God brought you into this world becomes stronger as you grow and then you die again.