---
layout: post
title: "Can we ever?"
date: 2018-06-06 14:25:38 -0700
category: post
---
She had just had a nightmare. But this time it was different. It wasn’t the usual running away from her co-workers, and work. But it was unusual, since she succeeded; not by running away but rather there were no co-workers or work. That didn’t happen in a very hypnotic way, it just was. So, it wasn’t a nightmare really, she thought, but in a way it was. What will she do? Work never really ends. Does it? There is always more to do. Even if you like your job a lot, you still don’t like it every day. And her, she disliked her job. She  didn’t only not-like it, but disliked it. But since it wasn’t a nightmare, what woke her up?, she wondered.

Nevertheless she was awake now, at 5:07am. Usually she wakes up at 5:40am precisely, then goes for a workout. So she was still in her sleep time according to her schedule, which she had followed religiously for 5 years now. She likes to plan everything because, she thinks, if she doesn’t do it her hours will fall apart, which eventually will become days, weeks, months… years? (maybe). In any case, not able to use her time perfectly meant she is losing an incredibly important part of her life. That’s all life has for us, our time, isn’t it?

But today her sleep had betrayed the schedule that she worked hard to establish, teaching her sleep her schedule. She couldn’t fall asleep anymore but still she waited for the alarm to go off. Not that she needed alarm anymore, but she set it for a situation if her biological clock fails her. She got out of the bed and carried on with her perfect routine. It was perfect until she reached her office. No wonder everyone was so chaotic on her walk to the office today. Her office building had developed cracks, from this morning’s earthquake. Being an old building, everyone thought this will happen in the next earthquake except they imagined a crumbled building not a cracked one. Suddenly from the crowd her co-workers found her.

“Hey, looks like we won’t have to work today”, said one.
“Is your apartment building ok? Are you ok? Did you feel it?”, asked another, worried about his boss.
“Yeah! Everything is fine”, wondering if it was the earthquake that woke her up.

As crazy as it may sound, it felt. This building was the only one in the city that was affected by the earthquake, which was just of magnitude 3 on richter scale. The city had seen worse, but apparently this was the last straw for that building.

Most of her colleagues didn’t seem affected at all. They were though, they were happy. They had a paid vacation basically, until the regional managers of the company were busy acquiring a new office space in the city. Nobody had been hurt in the event, so the city didn’t have a phase of resilience, residents were glad about that. Everything went on as it would. Her colleagues were happy and made plans to have a vacation, visit their family or friends, or just catch up on sleep or books. But she was upset, not only not-happy but unhappy to the point where she couldn’t stop thinking.

Depending on the new office location, she had to find a new place, move her stuff. More than that, she was worried if she will get a place with bedroom window facing east, so the sun could wake her up; if she will get a place from where she can just walk to the office; if she will find a place with a coffee shop with coffee she likes and ambience that she liked to read in, on weekend afternoons; if she will find a place that has small backyard for her composting setup; or if she should move at all? Sure she could just use the city metro service to work. But that meant having to leave for the office earlier than she did, waiting for the train; and what to do while waiting? And would she have to wake up earlier as well to leave early? What about her sleep schedule? …and on and on the thoughts went.

And there she sat looking out the window, while her colleagues enjoyed the days. She wasn’t running away from her co-workers. There were no co-workers, no work either. No job to dislike. Just like the dream that morning. She spend the time worrying about these immediate future decisions she had to make. _Spent_? rather wasted time. The order she setup was upset now, disturbed as she never planned for the disturbance. **But can we ever?**