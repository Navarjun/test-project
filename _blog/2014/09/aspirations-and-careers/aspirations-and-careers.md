---
layout: post
title:  "Aspirations and careers"
date:   2014-09-29 01:35:38 +0530
category: post
tags: ["letter-to-self"]
---

Past few weeks have been a roller coaster ride for me. I just left the job in July, I thought of starting up my own venture as soon as I am back from my 1 week travel plans. But then, those travel plans extended to about a month. And without a doubt I enjoyed them a lot, it was fun and I have no regrets for that. I was happy, about to pick up my gear and set myself up in a serene place away from the disturbances and coziness of home and start-up.

But as soon as I started talking about leaving the house for something like my own start-up(which I had already done once when I went to Bangalore), my mother started to fear for I was weak(as per her standards) and should stay at home for recovering my health. So, after trying to convince them, I learned that I would always fail. I stopped saying to go out(rarely though I mentioned that I wanted to go out). I just asked for a few things that would make it possible for me to work from home itself(which by-the-way is in a very small town).

After a few days though, my parents started again convincing me to go for further studies, as they really want me to get a Master Degree. I left the job to start my own venture, after me and my parents decided not to go for further studies. But a few weeks later I was back to square one. So, I didn’t want to go for further studies and I didn’t want to go abroad. But parents are parents you know. They worry a lot, they want me to be the best in whatever I do, they want me to avail all the opportunities that are not available here in India.

They really wish the best for me, so they always look for the safest path for me to travel and advise me to take that. They have always supported me in every way and I know they always will. I want them to be like a parachute, which saves us from hitting the ground badly in case we fall from the plane. Anyways, they are being the pilot of the plane, so I agreed to go for further studies and abroad.

But then came up another issue, according to which I didn’t want to study engineering further because I think I already can engineer stuff pretty well. So, I demanded to go for design(Interaction Design), but that too became the hot topic among our discussions, turning them into debates. According to them, art students study design(and arts is not a reputed area of study in India. Yes, in a country full of art, it’s not). I think that making great products has to be design and development. Being an computer engineer I can develop digital products really well, but designing got my mind tangled. I want to remove those tangles.

Anyways, I know parents will support me for anything I do. Today, they finally gave up on convincing to go for Master degree in Engineering, although half-heartedly. I know they love me and they want me to be successful.

So, I was discussing all this with one of my good friends. She asked me to clear up my confusions by talking to someone who understands me. Well, my blog understands me pretty well, I guess. So, here I am writing all my feelings that clears my mind. And when my mother reads this, she is going to kill me(because my parents have never actually stopped me from doing anything, always been very supportive, but it’s just the lack of awareness that makes them think that entrepreneurship/design won’t work. so they are worried).

I just wanna say, I’ll make you proud one day. Don’t worry. I may not know what’s best for me, but I will soon. That’s life isn’t it.
~Your son.