---
layout: post
title:  "The way idle-ists are"
date:   2014-05-08 19:55:38 +0530
category: post
---

I remember a story from the life and times of Alexander. It goes somewhat like this:

> When Alexander ruled whole of Persia(today’s middle-east), he thought of ruling India the next. He and his army was astonished by the grandeur of Himalayas. For them it was something that couldn’t have existed. To add to the astonishment, he met a gymnosophist. The gymnosophist was just lying around in the cool winds of the mountains, didn’t seem worried at all even though he had almost no clothing(to protect from cold) and no food. Alexander just asked him about his lifestyle. The gymnosophist just told him that he didn’t do anything at all and he thinks it’s fine. But to Alexander it was like wasting life.

So, there was a difference of world view between the gymnosophist and Alexander. Why?
For Alexander, it was just one life that he had, and everything he did mattered. In contrast, the gymnosophist thought that he had infinite lives(as per the mythologies he believed in). So, anything that Alexander did mattered, because he had ‘one’ as denominator where as for gymnosophist the denominator for all his deeds was 'infinity’: so all he did would come back to zero at last.

Was anyone wrong? Absolutely not. They both were correct as per their beliefs and understanding. But I believe that the gymnosophist was to a great extent more selfish than Alexander. I know, most people will disagree. Let me explain.

Alexander was in some sense creating jobs for all the soldiers, cooks, servants that he had, he was making a difference for those people. Gymnosophist was however a contradiction, he didn’t even save a dieing fish that was washed away by the wave of the river. I know that for Alexander the intentions were to rule the world, but for gymnosophist the intentions were to live his life(that was the end of it). Alexander was ready to take up challenges and fight for people he was ruling, that’s what a king does. But on the other hand gymnosophist didn’t even think of anyone else. He was content with what he had, and lived on food given to him as donations.

According to gymnosophist, there wasn’t any effect on the larger scheme of things of any of his actions. But really is that true? I guess not. What is the whole point of existence then? Each and everyone’s existence has effect on each and every atom existing in the universe.

Now, if you believe that, go out there and find a purpose for your lives. What way do you want the world to change and make your life worth the time you spend here.