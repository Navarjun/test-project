---
layout: post
title:  "Should I?"
date:   2015-05-12 21:12:38 +0530
category: post
tags: ["letter-to-self"]
---

I have started writing this, and about 10 times erased it and restarted it. That’s exactly what I am going through these days. A lot of confusion. Living alone has it’s own charm, gives you a lot of time to think, get in touch with lost friends. Having done that, you still have time left.

I don’t know about others but I start to think a lot about life, especially while reading biographies and memoirs. The idea of life makes me think a lot. What do to make it good. Universe is so large that even after 13.8 billion years after it’s origin, only a part of it is observable - which just means that distance that light travels in 13.8 billion years is not as long as the universe is vast. Are we even significant. People live their lives changing lives of other people - for good to for bad. But should it be all about us? Or should it be all about others?

We remember who influence our life directly or indirectly, be it Osama Bin Laden  or Mother Teresa. But for how long would we remember them. Everything dies eventually, even memories, probably books won’t.

Problem with me though is all about me and all about others seem to be the same. I try to help people not because they need help and it will make them feel better about it. But because, I will feel good about helping them; which makes me selfish I guess. So, I live both of the questions simultaneously.

Then they say intent matters. What about terrorists who killed Charlie Hebdo Cartoonists, they thought their intent was good - they were freeing world from a few more anti-islamists(I know I should not say that, they were just cartoonists.. but here it is referenced to what terrorists thought). So, what now?

There are a lot of questions like these which we probably will never understand, or answers would vary person to person. Why would I not want to have my own beliefs though is because I haven’t settled on one yet. Should I?