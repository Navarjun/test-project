---
layout: post
title:  "Where are we taking Liberty?"
date:   2017-09-23 14:25:38 -0500
category: post
tags: ""
---

With the end of World War II, the empires of colonial era started to break, giving rise to a lot of sovereign states liberating the residents of the planet. But those states were divided in different ways keeping the conflicts alive. These conflicts gave rise to modern day warfare, arguably even worse for the people living in conflict zones, making sure that liberty stays unequally/unfairly distributed.

Considering that even USA (self-proclaimed most liberal country) has compromises on distribution of liberty of people. Even though policies are in place for equality and fairness and justice, socially (in practice) it’s not like that. It’s still dependent on your social interactions. Most recently, this came to light when current president banned transgenders from serving in the army, which I consider to be an example of social inequality influencing policies just because biased people came to power.

## Role of internet

In 90’s internet started getting popular giving voice to people who socially are discriminated against. It gave rise to a lot of movements asking for equality. Most famous examples being the Black Lives Matter, Arab Spring, Feminist and LGBTQ movements. Turns out we needed them all along. On internet, people felt more liberty to say it out what they feel would not be listened to (or considered a taboo). People felt empowered when they found communities online with opinions similar to their own. They feel part of the society as internet enables them to find people like them which was previously almost impossible because of social and physical barriers.

> Even if someone is one in a million, there are 3600+ internet users like that person… internet users being more than 3.6 billion. So there is a very high likelihood of finding someone with similar opinions as you.

## The decline

The decline of trust in this family liberal place, called internet, started when people learned about hacking and Julian Assange told the world how our own data is not safe on internet. How far we are from controlling it. Later this fact was reinforced by Edward Snowden, and the trust was all lost. Statements from Facebook and Google didn’t help at all. Even normal people started to be scared of having a digital footprint (even now they are). No matter how much security each of the digital services provide, it’s hard to gain back that trust.

> It takes years to build trust. A moment to break it.

Services like WhatsApp, Signal and Telegram providing end-to-end encryption, making that their marketing strategy is the world we live in. Our data should be ours, security shouldn’t be a feature but a foundation. Trust of users like us goes away even further when CEO of Google comes to stage and lies by saying: “Farmer and Professor see the same results for same keywords”.

## The hope

Last year when Apple stood against the court’s orders and decided not to break into an iPhone, the hope in us came back. Even though they could do it, but making that software once would mean that it exists and anyone’s phone could be broken into. Tim Cook, CEO of Apple, told the press boldly that they have decided to defy the orders. A lot of Apple users felt pride in what Apple stands for once again (not everyone though). This debate turned into a revolution both for and against Apple. But what Apple was trying to do was important. Internet is one of the places (if not only) where everyone literally has equal rights. Apple was asking not to spoil that. Many companies including Google, Facebook and Amazon supported Apple giving us hope of controlling our own data hence encouraging liberty on internet. Because no control means we can’t express freely. But that hope wasn’t there to last long.

## The decline of hope

Recently Apple removed a lot of VPN apps from Chinese iPhone AppStore. This action was done to comply with China’s regional censorship laws. VPNs allow users within a firewall to pretend that they are outside the firewall, as a result avoid the firewall restrictions. In case of China, the Great Firewall has isolated Chinese people from the viewpoints of the rest of the world, stopping communication & hence taking away liberty of citizens to freely access internet as they please, restricting their access to internet (which won’t harm anyone and its against net neutrality). Controlling what information citizens are allowed to access is in my opinion taking away liberty (like in a prison, books and movies are scrutinized).

China is not only country doing it, Russia and Iran are other famous examples. Almost every country censors something, be it books, movies or internet. Censorship is the first step towards discouraging people’s right to information. The act of banning VPN services is a step further into discouraging that right. More importantly Apple is playing a role in it now. **And no-one is talking about it in the more liberal world as it doesn’t directly affect us. Hence taking liberty distribution a step towards colonial era.**