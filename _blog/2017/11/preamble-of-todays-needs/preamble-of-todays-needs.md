---
layout: post
title:  "Preamble of today's needs"
date:   2017-11-10 14:25:38 -0500
category: post
---

But even while Rome is burning,
There’s somehow time for shopping,
at IKEA…
See, when I moved out of the house earlier this week
toting my many personal belongings in large bins and boxes, and 50 gallon garbage boxes,
my first inclination was, of course, to purchase the things I still “needed” for my new place…
You know, just the basics, a shower curtain, a bed,
and, oh, I need a couch and a matching leather chair,
and a love seat, and a lamp, and a desk and a desk chair,
and another lamp for over there.
And, oh yeah, don’t forget about the sideboard,
that matches the desk and
a dresser for the bedroom.
And oh, I need a coffee table and a couple end tables,
and a TV stand for the TV I still need to buy.
And now that I think about it,
I’m going to want my apartment to be
my style, you know, my own motif.
So I need certain decorative to spruce up the decor.
But wait, what exactly is my style?
Do these stainless steel picture frames
embody that particular style?
Does this replica Matisse sketch
accurately capture my edgy but professional vibe?
Exactly how edgy am I?
What espresso maker defines me as a man?
Does the fact that I am asking these questions
preclude me from being a,
quote, “man’s man”?
How many cups, and plates,
and bowls should a man own.
I guess, I need a dining room table, too…
Right?
And a rug for the entryway, and bath mats.
What about that thing that’s sort of like a rug
but longer?
Yeah, a runner.
I’m gonna need one of those.
And I am also gonna need — 
Hell, what else do I need?