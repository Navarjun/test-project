---
layout: post
title:  "Christmas stickers"
date:   2016-12-23 14:25:38 -0500
category: post
tags: project
cover: "christmas-stickers.png"
---

Try the app [here][1].

A young man is enjoying a day of nothing to do, having some pain in his eyes after the fall semester. It’s winter, season’s first snowfall is awaited. In New England, people are waiting for Christmas. But this young man, has no awareness of it. Not only because his family isn’t Christian. But also because while the Christmas decorations were going up around Boston, he was busy with the end of semester obligations.

After procrastinating on a stroll in the open clean air of north Atlantic, for a long month, of final submissions at his grad school, he is ready to give himself a break. A break where he holds a cup of hot apple cider, of fresh apples from farms of the north-east, and walks around the city; starting next to Charles river. Looking at bare trees and fallen leaves, waiting for snow to cover them. Watching fat squirrels, layered up under their skin for Winter. The students, who are bustling in these parts of the city, aren’t there as vacation time is here.

He walks, no earphones, no camera, just enjoying the day of nothing to do while his eyes, neck, and shoulders recover from the fatigue. Often saying ‘hi’ or smiling to the people who still happen to be on streets, layered up. One thing he didn’t notice is the Christmas decorations. It’s only after dusk, that they become his centre of observation as they start glowing against the darkness rising behind them. They are beautiful, he thinks. People start showing up on the streets at this hour. Humans, he wonders, everywhere find something to celebrate.

The next day, he starts drawing some of the Christmas decorations that he can recall from his memory. Eventually, ends up drawing a pixel art Christmas tree. Then on, translating those random scribbles into digital illustrations, creating an iMessage app.

Try the app [here][2].

[1]: https://itunes.apple.com/us/app/christmas-stickers/id1326254152?ls=1&mt=8
[2]: https://itunes.apple.com/us/app/christmas-stickers/id1326254152?ls=1&mt=8

