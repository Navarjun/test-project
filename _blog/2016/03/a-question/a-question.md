---
layout: post
title:  "A Question"
date:   2016-03-13 21:14:38 +0530
category: post
tags: ["letter-to-self"]
---

There is always more to life,
The more we try,
The more it hides.

The better the color,
The better the price.
The more one pays,
The more one cries.

Sometimes I think,
Is it wise?
Sometimes about,
What is wise?

Where to go?
Or where to hide?
Rule the world,
Or simple life?