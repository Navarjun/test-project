var photoViewer = document.querySelector('.photo-viewer');
var allImages = document.querySelector('.photo-viewer .all-images');
var allImagesArr = [];
var activeIndex = -1;

document.querySelectorAll('.photo-item')
    .forEach(function(x, i) {
        var divEle = document.createElement('div');
        divEle.classList.add('image-icon');
        divEle.style.background = 'url('+x.dataset.imgurl+')';
        divEle.style.backgroundSize = 'cover';
        divEle.style.backgroundPosition = 'center';
        allImages.appendChild(divEle);

        divEle.onclick = function() {
            setActiveIndex(i);
        }

        allImagesArr.push(x.dataset.imgurl);

        x.onclick = function() {
            setActiveIndex(i);
        }
    });

document.querySelector('.right-arrow')
    .onclick = function(e) {
        setActiveIndex(activeIndex+1);
    }
document.querySelector('.left-arrow')
    .onclick = function(e) {
        setActiveIndex(activeIndex-1);
    }

function setActiveIndex(val) {
    if (val >= allImagesArr.length) {
        return;
    }
    activeIndex = val;
    if (val == -1) {
        photoViewer.style.opacity = 0;
        photoViewer.style.visibility = 'hidden';
    } else {
        photoViewer.querySelector('img.center-image').src = '/assets/images/pre-loader.gif';
        photoViewer.querySelector('img.center-image').src = allImagesArr[val];
        if (photoViewer.style.opacity != 1) {
            photoViewer.style.visibility = 'visible';
            photoViewer.style.opacity = 1;
        }
    }
    Array.from(allImages.children).forEach(function(x, i) {
        if (i == activeIndex) {
            x.classList.add('active');
        } else {
            x.classList.remove('active');
        }
    })
}

document.onkeydown = function(e) {
    // console.log(e.keyCode);
    
    if (activeIndex == -1) {
        return;
    }
    var keyCode = e.keyCode;
    // 37: left
    // 39: right
    // 27: escape
    switch (keyCode) {
        case 37:
            if (activeIndex == 0) {
                return;
            }
            setActiveIndex(activeIndex - 1);
            break;

        case 39:
            setActiveIndex(activeIndex + 1);
            break;
        case 27:
            setActiveIndex(-1);
    }
}

document.querySelector('.close-button')
    .onclick = function() {
        setActiveIndex(-1);
    }