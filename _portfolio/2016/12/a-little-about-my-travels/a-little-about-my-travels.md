---
layout: portfolio
title:  "A little about my travels"
cover: "three-trips.png"
date:   2016-12-15 14:25:38 -0500
category: post
tags: project
published: false
---

This project was done as part of a class taught by Douglass Scott at Northeastern University in fall 2016. The students were asked to design an infographic (poster or accordion) that visualizes trips from the place they currently live in. The first trip was the one that all students did everyday — from current place of residence to the Ryder Hall, which is where the class happened. The second trip was to somewhere that we have visited within the United States. The third trip was to somewhere outside the United States. We needed to tell 10 things about each trip in the infographic.
For my second trip, I imagined a trip that I would make from my current home to my home in India. For my third trip, I decided to visualize my visit to my cousin in San Jose California. I started sketching with pencil in accordion and poster form. Later decided to go with accordion form because a trip is a map and maps for going places are portable.

## First drafts

### Poster form

![hand draft details](hand-draft-details_s.jpg)

One point on the bottom-left of the paper is used as a starting point, current home, for all the 3 trips. The distance of each trip was abstracted in order to take advantage of the paper, since walking to Ryder Hall takes 15 minutes and flying to India takes 24 hours optimistically. Each trip has a different colored line which the viewer can follow to know more about the trip.
The lines go together as long as the route for the trips is the same and split when the route diverges. For example, at Boston Airport the green line continues to Houston Airport while the purple line goes in the other direction. The whole map uses a legend at the bottom-right to reference the mode of transportation in each part of the trip.

### Accordion form

![hand drawn draft](hand-draft_s.jpg)

The format is used in a way that each spread has one trip on it including origin and destination of the trip. Color of lines is used to differentiate between the mode of transport (foot, car, train, airplane) and what I was doing while traveling. The purple line represents my actions during the trip and blue ones represents the mode of transportation. Along with color different kinds of lines are being used to encode information.

But again, this form also uses a key (legend) to help the viewer understand the map better. According to our professor, keys are a crime in most cases. His opinion is that you never need a key unless you have an overwhelming amount of information to represent.

> If they have a key, it means they didnâ€™t do it correctly. Information should be right where the viewer is reading and they shouldnâ€™t have to look it up somewhere else on the paper unless itâ€™s absolutely neccessary.  
> ~ Douglass Scott

## Further Drafts

![critique](critique_s.jpg)

For the next iteration I definitely had to remove keys, his reason makes sense. I started drawing it in illustrator and improved upon some of the things, worked on my icons. I added the information close to the lines. But different kinds of lines were getting too confusing so I had to find some other way to represent that information. Some typographic mistakes also became apparent when I printed things out on a letter sized spread. But according to the feedback everyone suggested that the size was too small.
With the feedback I decided to increase the size to the tabloid size spread. But as soon as I did that I was unhappy with the portability of the project. It wasnâ€™t as portable as I initially imagined it to be. Then I tried to rethink the accordion form. After a bunch of sketches, I decided to use each page of the accordion as one step of the journey with additional information. Each page was square that folded behind the title page, each trip would start from the title page and open-up in different directions. The folded accordion was sized 10x10 cm.

![square accordion form](square-accordion_s.jpg)

The problem with this format was that the opened up accordion would be really long, and was not practical to print with available resources. Doug suggested me to decreased the size to 5x5 cm. But I went back to sketchbook. I decided to use the hexagonal-shaped accordion which gave me more space to add information on the paper rather than just one step on one sheet.

![final result](final_s.jpg)
