---
layout: portfolio
title:  "Hubway bike exploration"
date:   2017-03-08 14:25:38 -0700
category: post
tags: project
cover: "hubway.png"
excerpt: "This project tries to explore the Hubway bike sharing system's dataset from the bike's perspective. It explores one bike's trips around Greater Boston Area at once."
published: false
---

![Screenshot of the project website](screenshot_s.jpg)

[Project Link][1]

This project tries to explore the Hubway bike sharing system's dataset from the bike's perspective. It explores one bike's trips around Greater Boston Area at once. This was decided as there's a tendency to start seeing the general viewpoint of the Hubway system as a whole. However, I wanted to focus on bike's journey, as if bike was a person :). Every time you go to the website, it loads only one bike's data, which is randomly selected from the list of bikes that were used in 2016.

I started this project as I wanted to participate in Hubway data challenge 2017. But I fell sick just 3 days before the deadline and hence was unable to finish it. That's ok! I still finished it. This time I would not write too much, I would just leave you to explore the project. Let me know if you enjoy the project by tweeting [@Navarjun][2].

[Project Link][3]

The project code is available on [github][4].

> Bonus (if you are still reading)  
> If you find an interesting bike that you would like to share with your friends (I hope to be one of them), you can click the "share" button on the website, it would generate a link that would always lead to the website with the same bike's data. 

[1]: https://navarjun.github.io/hubway-data-2016/
[2]: http://twitter.com/navarjun
[3]: https://navarjun.github.io/hubway-data-2016/
[4]: https://github.com/Navarjun/hubway-data-2016-codebase