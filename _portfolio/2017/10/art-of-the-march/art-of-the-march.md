---
layout: portfolio
title:  "Art of the march"
date:   2017-10-07 14:25:38 -0500
cover: "cover_s.jpg"
excerpt: "This project is an archive of the posters that people carried and displayed at March for Women in Boston in 2016. It is a representation of political dissent of people. It represents a method of persuasion citizens try out in a civil/political discourse. This project was done in collaboration with Dietmar Offenhuber, Nathan Felde, Alessandra Renzi, and Siqi Zhu."
published: true
tags:
    - Data visualization
---

<!-- ![screenshot of the webpage](screenshot_s.jpg) -->

This project is an archive of the posters that people carried and displayed at March for Women in Boston in 2016. It is a representation of political dissent of people. It represents a method of persuasion citizens try out in a civil/political discourse. This project was done in collaboration with Dietmar Offenhuber, Nathan Felde, Alessandra Renzi, and Siqi Zhu.

The posters were placed (in a way mounted) on the fence of the Boston Common at the end of the march. These were collected by Dietmar Offenhuber, Nathan Felde, and Alessandra Renzi. Later on all of them were digitised (photographed). The entirety of this archive was made online, including the data that was created by manually tagging each of approximately 6000 posters. The project is still in progress. Check it out at [http://artofthemarch.boston ][1].

[1]: http://artofthemarch.boston